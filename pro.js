					//promise examples


// new Promise((resolve, reject) => {
// 	const d = new Date().getTime()
// 	const result = d % 2 === 0
// 	console.log("result", result)
// 	if (result === true) {
// 		resolve("Success")
// 	} else {
// 		reject("Fail")
// 	}
// }).then(data => {
// 	console.log("then block", data)
// 	return new Promise((resolve, reject) => resolve("2nd promise"))
// }).then(data => {
// 	console.log("2nd then", data)
// 	throw new Error("erro happened")
// }).catch(data => {
// 	console.log("catch block", data)
// })  

const promiserespo = new Promise((resolve, reject) => {
	const d = new Date().getTime()
	const result = d % 2 === 0
	if (result === true) {
		resolve("Success")
	} else {
		reject("Fail")
	}
})

const fn1 = async () => {
	try {
		const finalReponse = await promiserespo	
		console.log("inside fn1", finalReponse)
	} catch (error) {
		console.log("error", error)
	}
	
}

fn1()
 